%global add_to_license_files() \
        mkdir -p _license_files ; \
        cp -p %1 _license_files/$(echo '%1' | sed -e 's!/!.!g')

%global __provides_exclude_from ^(%{_libdir}/webkit2gtk-4\\.0/.*\\.so|%{_libdir}/webkit2gtk-4\\.1/.*\\.so|%{_libdir}/webkitgtk-6\\.0/.*\\.so)$
%global _lto_cflags %{nil}

%bcond_with docs

Summary:        GTK web content engine library
Name:           webkitgtk
Version:        2.46.1
Release:        2%{?dist}
License:        LGPLv2
URL:            https://www.webkitgtk.org/
Source0:        https://webkitgtk.org/releases/%{name}-%{version}.tar.xz

BuildRequires:  bison cmake flex gcc-c++ gettext git gperf ninja-build
BuildRequires:  bubblewrap hyphen-devel libatomic python3 xdg-dbus-proxy
BuildRequires:  perl(English) perl(FindBin) perl(JSON::PP) perl(bigint)
BuildRequires:  ruby rubygems rubygem-json
BuildRequires:  pkgconfig(atspi-2) pkgconfig(cairo) pkgconfig(egl)
BuildRequires:  pkgconfig(enchant-2) pkgconfig(fontconfig)
BuildRequires:  pkgconfig(freetype2) pkgconfig(gl) pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(glesv2) pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(gstreamer-1.0) pkgconfig(gstreamer-plugins-base-1.0)
BuildRequires:  pkgconfig(gtk+-3.0) pkgconfig(harfbuzz) pkgconfig(icu-uc)
BuildRequires:  pkgconfig(lcms2) pkgconfig(libgcrypt) pkgconfig(libjpeg)
BuildRequires:  pkgconfig(libnotify) pkgconfig(libopenjp2) pkgconfig(libpcre)
BuildRequires:  pkgconfig(libpng) pkgconfig(libseccomp) pkgconfig(libsecret-1)
BuildRequires:  pkgconfig(libsystemd) pkgconfig(libtasn1)
BuildRequires:  pkgconfig(libwebp) pkgconfig(libwoff2dec) pkgconfig(libxslt)
BuildRequires:  pkgconfig(sqlite3) pkgconfig(wayland-client) pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols) pkgconfig(wayland-server)
BuildRequires:  pkgconfig(wpe-1.0) pkgconfig(wpebackend-fdo-1.0) pkgconfig(xt)
BuildRequires:  pkgconfig(gbm) pkgconfig(libdrm) pkgconfig(libjxl) pkgconfig(epoxy)
BuildRequires:  pkgconfig(gstreamer-plugins-bad-1.0) unifdef
BuildRequires:  pkgconfig(gtk4) pkgconfig(libsoup-3.0)
BuildRequires:  pkgconfig(sysprof-capture-4)
%if %{with docs}
BuildRequires:  gi-docgen
%endif

%description
WebKitGTK is the port of the WebKit web rendering engine to the
GTK platform.

%package -n     webkitgtk6.0
Summary:        WebKitGTK for GTK 4
Requires:       javascriptcoregtk6.0 = %{version}-%{release}
Requires:       bubblewrap
Requires:       xdg-dbus-proxy
# Resolve conflicts with the webkit2gtk4.1 versions lower than 2.41.6-4
# We remove /usr/bin/WebKitWebDriver from webkit2gtk4.1 2.41.6-4
Requires:       (webkit2gtk4.1 >= 2.41.6-4 if webkit2gtk4.1 < 2.41.6-4)
Recommends:     geoclue2
Recommends:     gstreamer1-plugins-bad-free
Recommends:     gstreamer1-plugins-good
Recommends:     xdg-desktop-portal-gtk
Provides:       bundled(angle)
Provides:       bundled(pdfjs)
Provides:       bundled(xdgmime)
%ifnarch loongarch64
Provides:       bundled(skia)
%endif
Obsoletes:      webkit2gtk5.0 < %{version}-%{release}

%description -n webkitgtk6.0
WebKitGTK is the port of the WebKit web rendering engine to the
GTK platform. This package contains WebKitGTK for GTK 4.

%package -n     webkitgtk6.0-devel
Summary:        Development files for webkitgtk6.0
Requires:       webkitgtk6.0 = %{version}-%{release}
Requires:       javascriptcoregtk6.0 = %{version}-%{release}
Requires:       javascriptcoregtk6.0-devel = %{version}-%{release}
Obsoletes:      webkit2gtk5.0-devel < %{version}-%{release}

%description -n webkitgtk6.0-devel
The webkitgtk6.0-devel package contains libraries, build data, and header
files for developing applications that use webkitgtk6.0.

%package -n     javascriptcoregtk6.0
Summary:        JavaScript engine from webkitgtk6.0
Obsoletes:      javascriptcoregtk5.0 < %{version}-%{release}

%description -n javascriptcoregtk6.0
This package contains JavaScript engine from webkitgtk6.0.

%package -n     javascriptcoregtk6.0-devel
Summary:        Development files for JavaScript engine from webkitgtk6.0
Requires:       javascriptcoregtk6.0 = %{version}-%{release}
Obsoletes:      javascriptcoregtk5.0-devel < %{version}-%{release}

%description -n javascriptcoregtk6.0-devel
The javascriptcoregtk6.0-devel package contains libraries, build data, and header
files for developing applications that use JavaScript engine from webkitgtk-6.0.

%if %{with docs}
%package -n     webkitgtk6.0-doc
Summary:        Documentation files for webkitgtk6.0
Requires:       webkitgtk6.0 = %{version}-%{release}
Obsoletes:      webkit2gtk5.0-doc < %{version}-%{release}
BuildArch:      noarch

%description -n webkitgtk6.0-doc
This package contains developer documentation for webkitgtk6.0.
%endif

%prep
%autosetup -p1

%build
ncpus=%{_smp_build_ncpus}
maxcpus=$(( ($(awk '/^MemTotal:/{print $2}' /proc/meminfo)/1024/1024+1)/2*4/5))
if [ "$maxcpus" -ge 1 -a "$maxcpus" -lt "$ncpus" ]; then
    ncpus=$maxcpus
fi
%define _smp_mflags -j$ncpus

%ifarch aarch64
%global optflags %(echo %{optflags} | sed 's/-mbranch-protection=standard /-mbranch-protection=pac-ret /')
%endif

# https://bugs.webkit.org/show_bug.cgi?id=140176
%ifarch x86_64
%global optflags %(echo %{optflags} | sed 's/-g /-g1 /')
%endif

%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkitgtk-6.0
%cmake \
  -GNinja \
  -DPORT=GTK \
  -DCMAKE_BUILD_TYPE=Release \
  -DUSE_GTK4=ON \
  -DUSE_LIBBACKTRACE=OFF \
  -DENABLE_GAMEPAD=OFF \
%ifarch loongarch64
    -DUSE_SKIA=OFF \
%endif
%if %{without docs}
  -DENABLE_DOCUMENTATION=OFF \
%endif
  -DUSE_AVIF=OFF \
  %{nil}

%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkitgtk-6.0
export NINJA_STATUS="[webkitgtk-6.0][%f/%t %es] "
%cmake_build

%install
%define _vpath_builddir %{_vendor}-%{_target_os}-build/webkitgtk-6.0
%cmake_install
%find_lang WebKitGTK-6.0

%add_to_license_files Source/JavaScriptCore/COPYING.LIB
%add_to_license_files Source/ThirdParty/ANGLE/LICENSE
%add_to_license_files Source/ThirdParty/ANGLE/src/common/third_party/xxhash/LICENSE
%add_to_license_files Source/ThirdParty/ANGLE/src/third_party/libXNVCtrl/LICENSE
%add_to_license_files Source/WebCore/LICENSE-APPLE
%add_to_license_files Source/WebCore/LICENSE-LGPL-2
%add_to_license_files Source/WebCore/LICENSE-LGPL-2.1
%add_to_license_files Source/WebInspectorUI/UserInterface/External/CodeMirror/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/Esprima/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/three.js/LICENSE
%add_to_license_files Source/WTF/icu/LICENSE
%add_to_license_files Source/WTF/wtf/dtoa/COPYING
%add_to_license_files Source/WTF/wtf/dtoa/LICENSE

%files -n webkitgtk6.0 -f WebKitGTK-6.0.lang
%license _license_files/*ThirdParty*
%license _license_files/*WebCore*
%license _license_files/*WebInspectorUI*
%license _license_files/*WTF*
%{_libdir}/libwebkitgtk-6.0.so.4*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/WebKit-6.0.typelib
%{_libdir}/girepository-1.0/WebKitWebProcessExtension-6.0.typelib
%{_libdir}/webkitgtk-6.0/
%{_libexecdir}/webkitgtk-6.0/
%exclude %{_libexecdir}/webkitgtk-6.0/MiniBrowser
%exclude %{_libexecdir}/webkitgtk-6.0/jsc
%{_bindir}/WebKitWebDriver

%files -n webkitgtk6.0-devel
%{_libexecdir}/webkitgtk-6.0/MiniBrowser
%{_includedir}/webkitgtk-6.0/
%exclude %{_includedir}/webkitgtk-6.0/jsc
%{_libdir}/libwebkitgtk-6.0.so
%{_libdir}/pkgconfig/webkitgtk-6.0.pc
%{_libdir}/pkgconfig/webkitgtk-web-process-extension-6.0.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/WebKit-6.0.gir
%{_datadir}/gir-1.0/WebKitWebProcessExtension-6.0.gir

%files -n javascriptcoregtk6.0
%license _license_files/*JavaScriptCore*
%{_libdir}/libjavascriptcoregtk-6.0.so.1*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/JavaScriptCore-6.0.typelib

%files -n javascriptcoregtk6.0-devel
%{_libexecdir}/webkitgtk-6.0/jsc
%dir %{_includedir}/webkitgtk-6.0
%{_includedir}/webkitgtk-6.0/jsc/
%{_libdir}/libjavascriptcoregtk-6.0.so
%{_libdir}/pkgconfig/javascriptcoregtk-6.0.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/JavaScriptCore-6.0.gir

%if %{with docs}
%files -n webkitgtk6.0-doc
%dir %{_datadir}/gtk-doc
%dir %{_datadir}/gtk-doc/html
%{_datadir}/gtk-doc/html/javascriptcoregtk-6.0/
%{_datadir}/gtk-doc/html/webkitgtk-6.0/
%{_datadir}/gtk-doc/html/webkitgtk-web-process-extension-6.0/
%endif

%changelog
* Fri Dec 20 2024 Miaojun Dong <zoedong@tencent.com> - 2.46.1-2
- [Type] other
- [DESC] Rebuilt for icu

* Fri Oct 11 2024 Xinlong Chen <xinlongchen@tencent.com> - 2.46.1-1
- Upgrade to version 2.46.1

* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.44.3-2
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Thu Aug 29 2024 Xinlong Chen <xinlongchen@tencent.com> - 2.44.3-1
- Upgrade to version 2.44.3

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.44.2-2
- Rebuilt for loongarch release

* Mon Jul 22 2024 Xinlong Chen <xinlongchen@tencent.com> - 2.44.2-1
- Upgrade to version 2.44.2

* Tue Apr 23 2024 Rebuild Robot <rebot@opencloudos.org> - 2.44.1-2
- Rebuilt for gstreamer1-plugins-bad-free

* Mon Apr 15 2024 Fanjun Kong <fanjunkong@tencent.com> - 2.44.1-1
- Upgrade to version 2.44.1

* Wed Apr 10 2024 Fanjun Kong <fanjunkong@tencent.com> - 2.42.3-1
- Upgrade to upstream version 2.42.3
- Security fixes: CVE-2023-42916 CVE-2023-42917

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.41.6-6
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Aug 28 2023 Xiaojie Chen <jackxjchen@tencent.com> - 2.41.6-5
- Resolve conflicts with the webkit2gtk4.1 versions lower than 2.41.6-4

* Mon Aug 28 2023 Xiaojie Chen <jackxjchen@tencent.com> - 2.41.6-4
- Split into webkitgtk6.0

* Wed Aug 23 2023 rockerzhu <rockerzhu@tencent.com> - 2.41.6-3
- Rebuilt for icu 73.2

* Fri Aug 18 2023 Wang Guodong <gordonwwang@tencent.com> - 2.41.6-2
- Rebuilt for enchant2 2.5.0

* Tue Jul 18 2023 Xiaojie Chen <jackxjchen@tencent.com> - 2.41.6-1
- Upgrade to upstream version 2.41.6

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38.2-5
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.38.2-4
- Rebuilt for OpenCloudOS Stream 23

* Mon Feb 06 2023 Zhao Zhen <jeremiazhao@tencent.com> - 2.38.2-3
- Fixed bug of building error on arm64

* Wed Dec 07 2022 Xiaojie Chen <jackxjchen@tencent.com> - 2.38.2-2
- Enable gtk4 and libsoup3

* Mon Nov 21 2022 Xiaojie Chen <jackxjchen@tencent.com> - 2.38.2-1
- Initial build
